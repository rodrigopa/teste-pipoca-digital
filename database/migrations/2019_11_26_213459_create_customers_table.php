<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 45);
            $table->date('birth');
            $table->string('email', 45);
            $table->string('phone', 15);
            $table->string('cell_phone', 15);
            $table->string('cpf', 14);
            $table->string('photo');
            $table->boolean('status')->default(true);
            $table->string('city', 55);
            $table->string('address', 100);
            $table->string('complementary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
