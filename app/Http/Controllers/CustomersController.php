<?php

namespace App\Http\Controllers;

use App\Customer;

class CustomersController extends Controller
{
    public function index()
    {
        // Pegar registros do banco de dados atraveś do model
        $rows = Customer::get();

        // Retornar os dados como json
        return $rows;
    }
}
