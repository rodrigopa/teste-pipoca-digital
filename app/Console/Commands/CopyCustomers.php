<?php

namespace App\Console\Commands;

use App\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CopyCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy:customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy all data in temporary table customers to a new table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sqlFile = Storage::disk('local')->get('data.sql');

        // Importar data.sql
        DB::unprepared($sqlFile);

        // Executar comando para criar as migrations
        Artisan::call('migrate');

        // Executar comando para fazer a cópia dos registros
        DB::insert('INSERT INTO customers SELECT * FROM customers_temp');
    }
}
