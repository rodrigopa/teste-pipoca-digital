/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table customers_temp
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customers_temp`;

CREATE TABLE `customers_temp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth` date NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `city` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complementary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customers_temp` WRITE;
/*!40000 ALTER TABLE `customers_temp` DISABLE KEYS */;

INSERT INTO `customers_temp` (`id`, `name`, `birth`, `email`, `phone`, `cell_phone`, `cpf`, `photo`, `status`, `city`, `address`, `complementary`, `created_at`, `updated_at`)
VALUES
	(1,'Dr. Mateus Leon Rocha','2017-02-02','christian04@yahoo.com','(88) 90895-9901','(45) 98308-7349','575.600.094-17','https://lorempixel.com/100/100/?55813',0,'Santa Ivan','Av. Jasmin Brito, 64. F','Apto 517','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(2,'Sr. Andres Perez Beltrão Jr.','1998-05-29','grego.natal@deoliveira.com','(68) 98759-8635','(42) 96505-4655','416.695.178-53','https://lorempixel.com/100/100/?99024',1,'Ávila do Sul','Rua Simão Fernandes, 6. 89º Andar','1º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(3,'Regina Constância Lozano','2005-01-11','azevedo.alonso@correia.com','(47) 91262-6822','(47) 90342-7449','182.507.869-63','https://lorempixel.com/100/100/?20376',0,'Vila Cristóvão do Sul','Avenida Miranda Faria, 1','Bc. 3 Ap. 87','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(4,'Michele Adriana Galindo Sobrinho','1972-06-07','tomas61@dasilva.br','(53) 92522-1191','(46) 97135-4020','841.441.336-64','https://lorempixel.com/100/100/?44290',1,'Sérgio do Leste','Travessa Rafael Maldonado, 58','6º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(5,'Dr. Teobaldo Rivera','1985-01-16','ziraldo.valentin@dasneves.net','(15) 96320-6474','(86) 98814-8810','639.148.398-13','https://lorempixel.com/100/100/?38948',0,'Guerra d\'Oeste','Travessa Maitê, 115. Bc. 6 Ap. 99','Apto 316','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(6,'Dr. Alessandra Renata D\'ávila','1984-05-07','alonso74@mendonca.net','(93) 93440-3359','(49) 99267-7703','607.009.360-76','https://lorempixel.com/100/100/?79789',1,'Ashley do Sul','Rua Alma, 356. Fundos','4º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(7,'Sr. Thiago Fernando Gil Neto','1976-11-05','mteles@sepulveda.com','(31) 90943-3328','(66) 97045-8192','393.531.816-23','https://lorempixel.com/100/100/?82740',0,'Felipe do Leste','R. Violeta, 20. 6º Andar','5º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(8,'Guilherme Lutero Sobrinho','2012-01-10','tais94@assuncao.org','(54) 98768-9440','(46) 93450-1707','057.831.383-98','https://lorempixel.com/100/100/?61293',1,'Mascarenhas d\'Oeste','Largo Samanta, 1118','Bloco C','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(9,'Kevin Samuel Franco Neto','2004-02-23','adriana.gomes@pontes.com.br','(27) 95499-9049','(89) 92372-8198','246.421.001-30','https://lorempixel.com/100/100/?30200',1,'Vila Ricardo','Largo Elias Gusmão, 1707','Apto 6','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(10,'Bruno Jimenes','2008-04-21','rrico@ig.com.br','(66) 91546-2910','(88) 98477-2037','658.304.011-50','https://lorempixel.com/100/100/?10699',0,'Jácomo do Sul','Avenida das Dores, 2. Bc. 81 Ap. 39','Bc. 22 Ap. 28','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(11,'Sr. Everton Jimenes Jimenes','1997-03-23','kevin66@leal.com','(14) 96936-5981','(84) 94094-4478','289.105.633-70','https://lorempixel.com/100/100/?76076',0,'Santa Clara do Leste','Largo Maitê Zaragoça, 270. F','Bloco A','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(12,'Sr. Simão Agostinho Fontes','2009-06-27','salome75@gmail.com','(86) 93584-5004','(53) 95679-9223','741.325.057-82','https://lorempixel.com/100/100/?38509',1,'Pena do Norte','Av. Garcia, 3. Bloco B','Apto 9780','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(13,'Camilo Rico Bezerra','1974-01-20','svale@aragao.com.br','(96) 95693-9148','(82) 98457-4294','630.255.859-00','https://lorempixel.com/100/100/?51198',0,'Santa Máximo do Leste','Av. Fabiana Tamoio, 3160. Anexo','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(14,'Joaquin Cordeiro da Silva','1999-01-10','agostinho.chaves@campos.com','(84) 96341-6351','(88) 93437-8486','906.545.899-92','https://lorempixel.com/100/100/?35281',0,'Vasques d\'Oeste','Largo Christian Ramos, 88081. F','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(15,'Dante Martines Queirós','1985-05-01','sanches.natalia@r7.com','(51) 94104-5711','(82) 95682-0301','414.881.021-04','https://lorempixel.com/100/100/?82058',0,'Teobaldo do Sul','Rua Gonçalves, 592. Anexo','Apto 155','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(16,'Teobaldo Ferminiano Filho','1986-06-16','anderson14@gmail.com','(55) 94269-3539','(77) 99387-0854','184.071.929-02','https://lorempixel.com/100/100/?93619',1,'Francisco do Norte','Largo Demian Jimenes, 5. Fundos','Apto 8','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(17,'Sra. Violeta Barreto Sobrinho','1999-01-02','roque.alexandre@yahoo.com','(74) 91841-6229','(96) 93688-2573','774.436.371-01','https://lorempixel.com/100/100/?41979',0,'Porto Valéria','Largo Duarte, 76. 4º Andar','Bloco A','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(18,'Sra. Samanta Zaragoça Beltrão','2006-10-23','isabel59@uchoa.net','(16) 95691-7956','(87) 91518-1957','175.213.671-38','https://lorempixel.com/100/100/?10744',1,'Porto Joaquin','Av. de Oliveira, 20','Apto 6336','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(19,'Srta. Valentina Esteves','1987-06-09','aranda.luara@gomes.org','(68) 91653-7360','(35) 94136-4289','753.582.711-04','https://lorempixel.com/100/100/?26475',0,'Vila Paulina do Leste','Avenida Thiago, 55122. Fundos','Bc. 2 Ap. 68','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(20,'Sr. Aaron Ávila Chaves Jr.','2008-11-26','ivana.toledo@bezerra.org','(94) 97707-1384','(64) 99140-3628','416.837.692-36','https://lorempixel.com/100/100/?15665',1,'Cordeiro do Sul','Rua Pontes, 2716','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(21,'Sra. Michele Padrão Toledo','1990-10-07','delgado.luciano@velasques.com','(14) 96615-4150','(98) 92754-3306','418.792.156-75','https://lorempixel.com/100/100/?74079',0,'Noel do Norte','R. Amanda, 478','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(22,'Rebeca Corona Rezende Filho','2012-08-07','luana.solano@r7.com','(95) 92840-6612','(95) 92919-6717','185.564.774-53','https://lorempixel.com/100/100/?75397',0,'Faro d\'Oeste','Avenida Sara, 3369','Bc. 4 Ap. 98','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(23,'Simon Vasques','1976-09-24','tabata56@beltrao.com.br','(98) 94922-5234','(91) 94882-9068','842.457.402-80','https://lorempixel.com/100/100/?22565',0,'Brito do Norte','Av. Mascarenhas, 767','Apto 8113','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(24,'Srta. Hortência Regina Verdugo','2007-03-20','yserra@ramires.net','(41) 97000-4950','(98) 96847-4276','025.350.384-12','https://lorempixel.com/100/100/?78341',0,'São Sabrina','R. Sophie Batista, 9. Bc. 1 Ap. 16','Apto 5962','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(25,'Kevin Correia Medina Neto','1979-02-28','odeoliveira@ferraz.com.br','(74) 93464-2379','(22) 95054-3850','253.612.925-08','https://lorempixel.com/100/100/?16839',1,'Paes do Norte','Travessa Felipe, 85944','6º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(26,'Dr. Gian Caldeira Correia Neto','2019-04-25','mvelasques@ig.com.br','(18) 95744-4581','(44) 94296-9681','039.401.884-25','https://lorempixel.com/100/100/?75674',1,'São Taís','Avenida Jimenes, 98137. Bloco A','Bloco C','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(27,'Luara Sales','2002-03-24','miguel.vila@r7.com','(51) 91006-3266','(47) 98955-7391','109.885.555-81','https://lorempixel.com/100/100/?74462',0,'Porto Henrique','Travessa da Cruz, 67129','Apto 053','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(28,'Gustavo Christopher Carmona','1986-12-27','elizabeth36@terra.com.br','(67) 97882-6897','(73) 96970-8267','542.147.309-09','https://lorempixel.com/100/100/?88711',1,'Vila José do Norte','Rua Manuela, 2. Apto 151','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(29,'Sr. Manuel Feliciano','1996-03-02','guerra.alessandra@terra.com.br','(96) 92509-1790','(97) 97940-1259','111.622.073-37','https://lorempixel.com/100/100/?15908',0,'Julieta do Norte','Rua Quintana, 87. Bloco A','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(30,'Sr. Agostinho Henrique Faro Filho','2005-08-18','vicente.furtado@terra.com.br','(83) 93332-0711','(11) 97262-9345','037.124.302-56','https://lorempixel.com/100/100/?21617',0,'Carolina do Norte','Travessa Santana, 0732. 00º Andar','966º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(31,'Alessandra Dias Vila','2006-09-12','irene.barreto@prado.com','(98) 94744-0387','(84) 94727-6875','416.976.565-60','https://lorempixel.com/100/100/?87305',1,'Vila Aaron','R. Norma Estrada, 14','Apto 457','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(32,'Dr. Olívia Ramos Neto','2006-01-07','ddelgado@perez.net','(11) 94995-4867','(67) 98427-5649','966.044.750-77','https://lorempixel.com/100/100/?89091',1,'Diego do Norte','R. Mário Esteves, 42111. 98º Andar','Fundos','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(33,'Tomás Aranda Sepúlveda Filho','2011-03-02','mbenites@uol.com.br','(79) 99916-2471','(46) 98439-9472','439.263.910-06','https://lorempixel.com/100/100/?58369',1,'Taís do Sul','Rua Oliveira, 1371','Bc. 84 Ap. 93','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(34,'Sr. Sebastião D\'ávila Filho','2012-01-28','lozano.jacomo@uol.com.br','(62) 90014-7674','(66) 96715-0381','867.056.675-32','https://lorempixel.com/100/100/?83591',1,'Porto Laura do Norte','Av. Madalena, 842','Bc. 98 Ap. 06','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(35,'Sra. Mel Norma Franco Filho','1994-06-16','salgado.isadora@molina.net','(53) 99077-4928','(81) 98736-7710','896.214.583-90','https://lorempixel.com/100/100/?19370',0,'Santa David','Travessa Silvana, 94','Apto 43','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(36,'Juliana Romero Ferminiano','1994-07-31','romero.alexandre@delatorre.com.br','(83) 97304-7829','(89) 99063-4277','085.073.964-02','https://lorempixel.com/100/100/?90178',0,'Giovane do Sul','Avenida Mateus, 2467. Bloco B','8º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(37,'Sra. Maitê das Dores Matias Jr.','1973-03-22','serrano.constancia@gmail.com','(12) 98971-8522','(95) 90272-8007','346.899.105-37','https://lorempixel.com/100/100/?12656',1,'Suzana do Norte','Av. Grego, 78537','0º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(38,'Dr. Alexandre Camilo Rios','1970-01-07','mendes.carlos@delvalle.net','(89) 98129-8123','(38) 99814-6797','383.436.302-20','https://lorempixel.com/100/100/?41681',1,'Gomes do Leste','Av. Meireles, 6','Apto 5967','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(39,'Sra. Mariana Nádia Medina','1987-09-18','benites.estevao@uol.com.br','(93) 96963-1149','(43) 98780-5100','740.945.569-15','https://lorempixel.com/100/100/?82830',0,'da Cruz d\'Oeste','Rua Michele, 876. Bc. 6 Ap. 73','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(40,'Sara Branco Jr.','2017-09-16','padilha.felipe@uol.com.br','(97) 94748-0011','(38) 96914-2141','668.517.632-65','https://lorempixel.com/100/100/?75694',1,'Santa Isadora','Av. Ariadna, 3721. Bloco B','Bloco A','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(41,'Sra. Alma Caldeira Rangel Sobrinho','1970-11-28','luciana.vasques@terra.com.br','(87) 95420-2369','(64) 97345-6747','992.182.435-00','https://lorempixel.com/100/100/?61955',0,'Nicole d\'Oeste','Avenida Nicole Rodrigues, 389','Apto 3','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(42,'Malena Galhardo','2001-03-14','melissa87@r7.com','(93) 96146-7665','(89) 98417-7366','168.427.790-65','https://lorempixel.com/100/100/?95633',0,'São Eduardo','Avenida Molina, 415. Fundos','Apto 8','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(43,'Dr. Andrea Sabrina da Silva','1990-07-25','pavila@pena.net.br','(63) 96577-2197','(44) 90697-1685','433.577.690-00','https://lorempixel.com/100/100/?93228',0,'São Pâmela','R. Luara Guerra, 559. 5º Andar','86º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(44,'Dr. Ivan Maldonado Sobrinho','2018-09-01','elizabeth.uchoa@terra.com.br','(83) 92341-7860','(19) 93367-8561','489.930.771-34','https://lorempixel.com/100/100/?14872',1,'Porto Malena do Norte','Largo Campos, 400','564º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(45,'Dr. Gian Caldeira','1992-11-30','tteles@yahoo.com','(87) 98280-1883','(88) 90797-1674','243.269.465-12','https://lorempixel.com/100/100/?52391',1,'Vila Ana do Norte','Av. Solano, 3821. Bc. 1 Ap. 30','Bloco A','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(46,'Dr. Isaac Marés Santacruz','2002-05-20','esales@valencia.com.br','(65) 99029-2606','(74) 96202-5570','773.563.279-70','https://lorempixel.com/100/100/?82366',0,'Santa Natal','R. Teobaldo das Dores, 1810','Bc. 34 Ap. 44','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(47,'Mateus Isaac Ferreira Jr.','1987-08-28','teles.gabriel@barreto.net.br','(43) 90714-5595','(15) 99332-8010','388.980.571-05','https://lorempixel.com/100/100/?73439',1,'Santa Sérgio do Leste','Largo Daniel da Rosa, 68','7º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(48,'Sr. Aaron Velasques Sobrinho','2004-05-07','verdugo.alan@uol.com.br','(49) 96376-1537','(99) 93626-9836','528.964.784-85','https://lorempixel.com/100/100/?84913',0,'Fonseca d\'Oeste','Rua Cordeiro, 09','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(49,'Dr. Ian Estrada','2014-10-13','hugo.ferminiano@fonseca.net.br','(21) 92906-3796','(99) 99597-1538','600.933.023-80','https://lorempixel.com/100/100/?70450',0,'Santa João d\'Oeste','Largo Pablo, 1749','Fundos','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(50,'Evandro Everton Jimenes','1978-04-15','rocha.constancia@terra.com.br','(98) 93695-8342','(82) 99343-5844','782.936.719-12','https://lorempixel.com/100/100/?25865',1,'São Elias','Avenida Verdugo, 7053','Bc. 35 Ap. 13','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(51,'Luana Benites Dominato','1998-01-31','pontes.ricardo@ig.com.br','(34) 95724-2496','(54) 92324-7417','794.362.500-53','https://lorempixel.com/100/100/?23913',1,'D\'ávila do Norte','Av. Alexa, 317','Apto 4594','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(52,'Dr. Horácio Romero Assunção','1979-04-13','fonseca.christian@galindo.org','(37) 96061-5201','(27) 96476-3854','706.797.667-74','https://lorempixel.com/100/100/?40185',0,'Salas do Norte','Av. Aranda, 1. Bloco A','Bc. 0 Ap. 14','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(53,'Daniela Tábata Sandoval','1991-03-04','xdelatorre@guerra.com.br','(63) 95139-8676','(97) 97869-8546','898.321.451-12','https://lorempixel.com/100/100/?50023',0,'Vila Emília do Leste','R. Renata, 6. 264º Andar','6º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(54,'Sr. Ian Pena','1985-06-04','hortencia.dasneves@rico.org','(14) 99037-8485','(92) 99043-1495','253.123.116-18','https://lorempixel.com/100/100/?60646',1,'Meireles do Leste','Av. Ziraldo, 68','909º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(55,'Dr. Alessandra Escobar Rezende Sobrinho','1994-10-23','elias59@hotmail.com','(28) 91611-8181','(51) 91639-1498','030.185.070-45','https://lorempixel.com/100/100/?38779',0,'Vila Ian d\'Oeste','Av. João, 86905. 9º Andar','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(56,'Leandro Ortega Sobrinho','2011-08-03','maia.francisco@ig.com.br','(35) 94013-2171','(44) 97736-9964','208.666.630-61','https://lorempixel.com/100/100/?84143',1,'Porto Josefina','Rua D\'ávila, 4. Apto 1798','Apto 373','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(57,'Srta. Antonieta Mascarenhas Barros','2007-08-31','sebastiao26@sanches.com.br','(73) 93835-4426','(12) 92509-7582','929.669.962-26','https://lorempixel.com/100/100/?57910',0,'Noel d\'Oeste','Avenida Vasques, 04','Bc. 15 Ap. 40','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(58,'Dr. Micaela Adriana Furtado','2017-07-07','matos.joana@vila.com','(38) 90148-3177','(55) 97901-0349','238.492.016-27','https://lorempixel.com/100/100/?52126',0,'Gil do Norte','Av. Flores, 3','Bc. 11 Ap. 83','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(59,'Amélia Giovana das Dores','1973-01-23','solano.josue@ig.com.br','(19) 97496-4348','(15) 93900-6516','672.563.705-12','https://lorempixel.com/100/100/?19161',1,'Fonseca do Norte','Largo Balestero, 1422','Apto 571','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(60,'Violeta Pedrosa Escobar','1980-04-12','jacomo.domingues@duarte.net','(43) 92413-7631','(61) 97747-6499','578.278.189-00','https://lorempixel.com/100/100/?90546',1,'Porto Ivana','Largo Valentin Verdugo, 8','Bc. 1 Ap. 31','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(61,'Sra. Valentina Valentin Filho','1975-05-10','yqueiros@rangel.br','(37) 96227-6528','(48) 91204-3899','103.629.731-48','https://lorempixel.com/100/100/?73483',0,'Porto Luzia do Leste','Avenida Renata, 89662','Apto 4830','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(62,'Violeta Luciana Bittencourt Filho','1983-02-26','agustina28@deaguiar.com','(12) 98772-1155','(38) 93358-1967','386.972.688-18','https://lorempixel.com/100/100/?78980',1,'Gonçalves do Leste','Av. Giovana Tamoio, 65343','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(63,'Dr. Kevin Santiago','2010-07-06','arangel@lovato.org','(24) 94182-8789','(32) 98814-2049','377.171.934-85','https://lorempixel.com/100/100/?69040',1,'Porto Manuela','Rua Paula Paz, 7081','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(64,'Allison da Rosa Salgado','2006-09-05','marinho.fabiana@gmail.com','(92) 94355-8617','(38) 92144-2357','934.974.628-02','https://lorempixel.com/100/100/?66729',0,'São Bianca d\'Oeste','Rua Isaac, 2','Bc. 73 Ap. 72','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(65,'Samuel Mendes das Neves','1982-10-12','hernani79@balestero.br','(85) 97747-6463','(95) 92470-2633','750.505.488-02','https://lorempixel.com/100/100/?87904',0,'Ian d\'Oeste','Travessa Samuel Branco, 04788','Apto 9','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(66,'Luciana Valentin Duarte','2003-09-27','michele67@delatorre.com','(35) 97711-8071','(67) 93087-0445','974.414.492-00','https://lorempixel.com/100/100/?14641',1,'Luara do Leste','Largo Diego, 18','Apto 025','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(67,'Melissa Serra Vieira','1977-09-05','caldeira.anderson@gmail.com','(53) 93552-0696','(55) 98399-3657','457.403.201-43','https://lorempixel.com/100/100/?77797',1,'São Julieta d\'Oeste','Rua Sérgio Espinoza, 20. Anexo','Apto 5735','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(68,'Dr. Josué Fábio Santana','1985-12-18','zcaldeira@garcia.com.br','(92) 94438-8251','(74) 99530-9033','377.216.805-14','https://lorempixel.com/100/100/?50876',0,'Santa Giovane','R. Constância, 1. Apto 428','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(69,'Sophie Josefina Azevedo Neto','2010-02-06','antonieta24@ig.com.br','(43) 98873-0960','(49) 93284-5088','209.217.181-01','https://lorempixel.com/100/100/?67096',0,'Renata do Norte','Av. Ketlin, 44. Bloco A','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(70,'Srta. Luna Lovato','1995-12-27','juliana03@valentin.br','(73) 90227-9447','(83) 97308-3932','772.014.678-67','https://lorempixel.com/100/100/?42414',1,'Renata d\'Oeste','Rua Isabel, 9083. F','Apto 67','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(71,'Paula Escobar Neto','2006-03-28','xzamana@vega.net','(33) 95451-8777','(62) 90672-7193','052.828.729-00','https://lorempixel.com/100/100/?77287',1,'Galvão do Sul','Av. Bezerra, 059. 71º Andar','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(72,'Pedro Daniel Rosa Sobrinho','1991-01-03','paula.uchoa@yahoo.com','(73) 93123-3951','(81) 93568-8123','289.593.322-75','https://lorempixel.com/100/100/?55310',1,'Madeira do Leste','Av. Estêvão Delatorre, 2','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(73,'Sr. Gian da Rosa Sobrinho','2015-08-02','samanta67@camacho.com','(48) 92658-3874','(81) 99637-3043','683.735.803-01','https://lorempixel.com/100/100/?83465',1,'Julieta do Sul','Travessa Alonso Serra, 22','Apto 3','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(74,'Dr. Irene Samanta Valência','2011-05-03','uchoa.jose@r7.com','(77) 91135-1720','(44) 95666-8962','664.536.518-74','https://lorempixel.com/100/100/?22506',0,'Santa Melissa do Leste','Av. Amanda de Souza, 7. Bc. 7 Ap. 86','Bloco B','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(75,'Sr. Manuel Valência Filho','1983-04-22','vleon@uol.com.br','(61) 93068-1227','(32) 92961-0607','939.445.113-79','https://lorempixel.com/100/100/?13470',0,'Nádia do Sul','R. Samanta Serna, 90. Anexo','25º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(76,'Sra. Sophie Ariana Urias Filho','1985-07-29','drosa@carrara.com.br','(18) 93130-9259','(28) 93743-4425','923.840.887-42','https://lorempixel.com/100/100/?50190',1,'Daniela do Leste','Avenida Valentin, 96','Anexo','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(77,'Gabriela Leal Torres','2000-01-29','amanda.sepulveda@urias.br','(64) 91810-6232','(65) 90141-8038','684.307.285-20','https://lorempixel.com/100/100/?19977',1,'Porto Ricardo do Leste','Av. Vale, 504','Fundos','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(78,'Madalena Galindo da Rosa Jr.','1972-08-16','ariadna.cervantes@r7.com','(94) 90512-1139','(62) 93633-7296','436.168.822-22','https://lorempixel.com/100/100/?97545',1,'Cruz d\'Oeste','Avenida Daniel, 364','Bc. 08 Ap. 63','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(79,'Natália Violeta Aguiar','1992-06-08','oassuncao@rangel.com.br','(35) 98229-0262','(94) 95142-2662','796.910.100-38','https://lorempixel.com/100/100/?68558',0,'Eduardo do Norte','R. Serrano, 84','04º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(80,'Dr. Júlia Vasques Balestero Filho','2012-08-13','npacheco@uol.com.br','(21) 93664-0518','(42) 97910-4880','737.641.158-58','https://lorempixel.com/100/100/?16582',1,'Gian do Sul','R. Emiliano, 39758. 498º Andar','876º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(81,'Luana Lira Ferreira','2009-09-25','adriano.queiros@hotmail.com','(15) 99066-7781','(32) 97036-6558','325.061.217-06','https://lorempixel.com/100/100/?68184',0,'Sepúlveda d\'Oeste','R. Vitória Mendonça, 942','48º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(82,'Josué Queirós','1995-02-02','oroque@cervantes.org','(73) 98859-1593','(34) 96253-7888','172.860.011-16','https://lorempixel.com/100/100/?45314',0,'Casanova d\'Oeste','Travessa Noel Oliveira, 0. Bloco A','Apto 0129','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(83,'Sr. Eduardo Miguel Benites Sobrinho','1985-08-13','sophie49@gmail.com','(68) 99179-3260','(45) 97844-7260','813.967.259-97','https://lorempixel.com/100/100/?95049',0,'Abreu do Sul','R. Suzana, 8066','Bc. 8 Ap. 62','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(84,'Noelí Ávila Rangel Jr.','2001-10-04','melissa80@gmail.com','(84) 99990-7152','(34) 98508-8206','692.973.076-98','https://lorempixel.com/100/100/?39907',1,'Hugo do Sul','Travessa Luna, 12','Fundos','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(85,'Hernani Leonardo Espinoza','2014-05-19','ornela.vieira@aguiar.com.br','(35) 90263-9912','(45) 90605-6579','609.115.562-81','https://lorempixel.com/100/100/?73212',0,'Santiago do Sul','Largo Oliveira, 785. Anexo','Bc. 88 Ap. 09','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(86,'Pablo Ferraz Fernandes Neto','1999-04-27','valencia.josue@sepulveda.com','(74) 92383-6743','(92) 90158-7760','288.105.230-40','https://lorempixel.com/100/100/?67044',1,'Santa Salomé','R. Everton, 6','Bc. 8 Ap. 98','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(87,'Noel Balestero Barros Jr.','1975-06-07','tverdugo@yahoo.com','(98) 91378-9153','(71) 90380-5647','898.254.096-22','https://lorempixel.com/100/100/?99384',0,'Vila Ziraldo do Leste','Av. Daniel Valentin, 204. Bloco B','Apto 554','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(88,'Mariana Sales Zaragoça','2000-08-01','adriana13@mares.br','(95) 93737-2728','(35) 94493-9948','250.132.643-16','https://lorempixel.com/100/100/?39621',1,'Santa Guilherme','Av. Fonseca, 882. Bloco A','Apto 23','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(89,'Thalissa Matias Uchoa Filho','1998-05-24','manuela76@gmail.com','(82) 94716-7091','(43) 95077-1227','412.819.991-56','https://lorempixel.com/100/100/?75984',0,'Porto Ariadna','R. Sérgio Salas, 15430. Bloco C','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(90,'Ian Gusmão Faria','2006-03-03','jorge.salgado@uol.com.br','(48) 94708-7280','(51) 92205-8849','707.209.671-04','https://lorempixel.com/100/100/?43896',0,'São Violeta do Sul','R. Prado, 2977','Apto 330','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(91,'Regina Teles Salas Sobrinho','1985-04-14','joaquin.balestero@serrano.br','(47) 99215-0689','(63) 90766-7733','275.025.059-54','https://lorempixel.com/100/100/?14054',1,'Fonseca do Norte','Rua Salgado, 1157. Bloco B','Apto 05','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(92,'Leonardo Maximiano Valentin','1981-12-20','olivia.rangel@marinho.com','(17) 97332-0775','(63) 92392-2901','818.711.757-55','https://lorempixel.com/100/100/?75150',0,'Santa Mário do Leste','Travessa Elizabeth Fernandes, 3283','Bc. 80 Ap. 01','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(93,'Tomás Velasques Mendonça','2000-01-16','vramires@camacho.net.br','(77) 95277-7560','(62) 96713-5579','969.177.279-54','https://lorempixel.com/100/100/?72038',1,'São Carlos do Sul','Travessa Rosa, 510','Apto 01','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(94,'Antonieta Matias Serrano','2014-12-05','evalencia@terra.com.br','(63) 92058-8400','(35) 96570-3137','055.884.370-04','https://lorempixel.com/100/100/?61781',1,'Marin do Leste','Largo Jimenes, 9. Bc. 89 Ap. 13','Apto 630','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(95,'Bruno Demian Beltrão Neto','1975-01-09','gustavo90@quintana.net','(21) 95799-3141','(33) 91309-5132','145.527.362-72','https://lorempixel.com/100/100/?13082',1,'de Aguiar do Leste','Av. Regina Serna, 2145. Bc. 4 Ap. 28','F','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(96,'Adriana Isabella Chaves Neto','2019-09-18','luzia.caldeira@queiros.com','(31) 90027-0649','(66) 97857-8928','393.952.900-13','https://lorempixel.com/100/100/?51254',0,'Porto Mateus','Av. Demian Oliveira, 932','Bc. 1 Ap. 78','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(97,'Luciana Agustina Queirós Neto','1995-11-03','irene.saraiva@salazar.com','(49) 91268-0091','(32) 98363-1459','866.566.281-25','https://lorempixel.com/100/100/?42885',1,'Pablo do Norte','Largo Quintana, 4753. Bloco C','Bloco C','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(98,'Sr. Daniel Lozano','1986-08-29','ucortes@mendes.br','(27) 93338-9305','(37) 90256-8831','588.972.283-21','https://lorempixel.com/100/100/?72146',1,'São Sérgio','R. Padrão, 0125','Bc. 18 Ap. 70','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(99,'Dr. Diego Marques Fontes Neto','1981-09-23','maite.grego@terra.com.br','(94) 99352-9260','(81) 98481-2393','170.984.937-16','https://lorempixel.com/100/100/?16308',0,'Casanova d\'Oeste','Avenida Simon, 0','539º Andar','2019-11-21 15:07:36','2019-11-21 15:07:36'),
	(100,'Carlos Espinoza Gil Filho','1973-02-09','sofia.fidalgo@ig.com.br','(13) 92845-1888','(67) 90970-1308','765.990.282-25','https://lorempixel.com/100/100/?54932',0,'Porto Luis do Sul','Av. Reis, 84','Apto 11','2019-11-21 15:07:36','2019-11-21 15:07:36');

/*!40000 ALTER TABLE `customers_temp` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
